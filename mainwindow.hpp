#ifndef MAINWINDOW_HPP
#define MAINWINDOW_HPP

#include <iostream>
#include <fstream>
#include <string>

#include <QMainWindow>
#include <QTcpSocket>
#include <QHostAddress>
#include <QKeyEvent>
#include <QComboBox>
#include <QByteArray>
#include <QtCore/QObject>
#include <QtCore/QTimer>
#include <QDate>
#include <QFile>
#include <QDir>
#include <QTextStream>
#include <QCoreApplication>
#include <QDir>


#define CMD_START 10
#define CMD_STOP 11

#define MODE_AUTO 20
#define MODE_MANUAL 21

#define MOVE_HALT 30
#define MOVE_GO_FORWARD 31
#define MOVE_GO_BACKWARD 32
#define MOVE_TURN_LEFT 33
#define MOVE_TURN_RIGHT 34


QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

using namespace std;

class MainWindow : public QMainWindow{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

public slots:
    void goForward();
    void goBackward();
    void turnLeft();
    void turnRight();
    void onReadyRead();
    void write(QByteArray msg);
    void startStop();
    void switchMode();
    void reconnect();
    void switchState();
    void modifyIP();
    void saveLogFonction();


protected:
    void keyPressEvent(QKeyEvent *);
    void keyReleaseEvent(QKeyEvent *);
    void mousePressEvent(QMouseEvent *);

private:
    Ui::MainWindow *ui;
    QTcpSocket m_socket;

    bool m_isRunning;
    bool m_autoMode;

    QString m_IPAddress;
    int m_TCPPORT;
    QFile m_logFile;
};
#endif // MAINWINDOW_HPP
