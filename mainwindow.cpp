#include "mainwindow.hpp"
#include "ui_mainwindow.h"

//Constructor
MainWindow::MainWindow(QWidget *parent)
    : QMainWindow{parent},
      ui{new Ui::MainWindow},
      m_socket{this},
      m_isRunning{true},
      m_autoMode{false},
      m_IPAddress{"192.168.1.43"}, //addresse par défaut (Si à changer : modifier le texte par défaut dans l'ui).
      m_TCPPORT{51717}
{
    QDir logDirectory;

    //change le chemin du fichier de log en fonction de l'OS
    if(QSysInfo::productType() == "osx"){
        logDirectory.mkpath(QCoreApplication::applicationDirPath()+"/../../../logs"); //créé le dossier de log s'il n'éxiste pas encore.
        m_logFile.setFileName(QCoreApplication::applicationDirPath() + "/../../../logs/" + QDateTime::currentDateTime().toString("yyyy-MM-dd_hh_mm_ss") + ".log");
    }
    else{
        logDirectory.mkpath(QCoreApplication::applicationDirPath()+"/logs"); //créé le dossier de log s'il n'éxiste pas encore.
        m_logFile.setFileName(QCoreApplication::applicationDirPath() + "/logs/" + QDateTime::currentDateTime().toString("yyyy-MM-dd_hh_mm_ss") + ".log");
    }

    //initialisation de tout l'interface
    ui->setupUi(this);

    //Fixe les règles pour la saisie des addresses IP.
    QRegExp IPRegExp{ "^(?:[0-9]{1,3}\.){3}[0-9]{1,3}$" };
    QValidator *validator = new QRegExpValidator(IPRegExp, this);
    ui->GetIP->setValidator(validator);
    ui->GetIP->setMaxLength(15);
    ui->GetIP->setEnabled(false);

    //connection du superviseur au serveur
    m_socket.connectToHost(QHostAddress(m_IPAddress), m_TCPPORT);

    //Connection entre les signaux et les slots.
    connect(&m_socket, SIGNAL(readyRead()), this, SLOT(onReadyRead()));
    connect(ui->StopButton, SIGNAL(pressed()), this, SLOT(startStop()));
    connect(ui->Mode, SIGNAL(pressed()), this, SLOT(switchMode()));
    connect(&m_socket, SIGNAL(stateChanged(QAbstractSocket::SocketState)), this, SLOT(switchState()));
    connect(ui->reconnect, SIGNAL(pressed()), this, SLOT(reconnect()));
    connect(ui->GetIP, SIGNAL(editingFinished()), this, SLOT(modifyIP()));
    connect(ui->saveLog, SIGNAL(stateChanged(int)), this, SLOT(saveLogFonction()));
}

//Destructeur
MainWindow::~MainWindow()
{
    delete ui;
    if(m_logFile.isOpen()){
        m_logFile.close();
    }
}


//Fonction appelée à chaque appuie sur une touche du clavier.
void MainWindow::keyPressEvent(QKeyEvent *event)
{
    if(!m_autoMode){
        switch (event->key()) {
            case Qt::Key_W :
                if(!event->isAutoRepeat()) //vérifie que le signal ne vient pas de la répétition automatique du clavier.
                    goForward();
                break;
            case Qt::Key_S :
                if(!event->isAutoRepeat())
                    goBackward();
                break;
            case Qt::Key_A :
                if(!event->isAutoRepeat())
                    turnLeft();
                break;
            case Qt::Key_D :
                if(!event->isAutoRepeat())
                    turnRight();
                break;
            case Qt::Key_Up :
                if(!event->isAutoRepeat())
                    goForward();
                break;
            case Qt::Key_Down :
                if(!event->isAutoRepeat())
                    goBackward();
                break;
            case Qt::Key_Left :
                if(!event->isAutoRepeat())
                    turnLeft();
                break;
            case Qt::Key_Right :
                if(!event->isAutoRepeat())
                    turnRight();
                break;

            default :
                break;
        }
    }
}

//Fonction appelée à chaque relachement d'une touche du clavier.
void MainWindow::keyReleaseEvent(QKeyEvent *event)
{
    QByteArray data;
    if(!m_autoMode){
        switch (event->key()) {
            case Qt::Key_W :
            case Qt::Key_S :
            case Qt::Key_A :
            case Qt::Key_D :
            case Qt::Key_Up :
            case Qt::Key_Down :
            case Qt::Key_Left :
            case Qt::Key_Right :
                data.push_front(MOVE_HALT);
                write(data);
                break;

            case Qt::Key_L :
                ui->console->appendPlainText("It is Wednesday my dudes");

                //Vérifie si l'option "savelog" est coché et si oui, ecrit dans log.txt
                if(ui->saveLog->isChecked()){
                    if(m_logFile.isOpen()){
                        QTextStream stream(&m_logFile);
                        stream << "It is Wednesday my dudes" << endl;
                    }
                }
                break;

            case Qt::Key_M :
                ui->console->appendPlainText("aaaaaaaaaaaaaAAAAAAAAAAAAAAAHHH");

                //Vérifie si l'option "savelog" est coché et si oui, ecrit dans log.txt
                if(ui->saveLog->isChecked()){
                    if(m_logFile.isOpen()){
                        QTextStream stream(&m_logFile);
                        stream << "aaaaaaaaaaaaaAAAAAAAAAAAAAAAHHH" << endl;
                    }
                }
            break;

            default :
                break;
        }
    }
}

//Fonction appelée à chaque clic de souris.
void MainWindow::mousePressEvent(QMouseEvent */*event*/){
    ui->GetIP->setEnabled(true); //Réactive la zone de saisie de l'addresse IP lorsque l'on clique dessus.
};


//Envoie la commande "Avance"
void MainWindow::goForward(){
    QByteArray data;
    data.push_front(MOVE_GO_FORWARD);
    write(data);
}


//Envoie la commande "Recule"
void MainWindow::goBackward(){
    QByteArray data;
    data.push_front(MOVE_GO_BACKWARD);
    write(data);
}


//Envoie la commande "Tourne à gauche"
void MainWindow::turnLeft(){
    QByteArray data;
    data.push_front(MOVE_TURN_LEFT);
    write(data);
}

//Envoie la commande "Tourne à droite"
void MainWindow::turnRight(){
    QByteArray data;
    data.push_front(MOVE_TURN_RIGHT);
    write(data);
}


//Fonction en charge de récupérer les messages du serveur.
void MainWindow::onReadyRead(){
    QByteArray tempo = m_socket.readAll();
    std::string data{tempo.data()};
    if(data.front() == '0'){
        //Message de log
        QString text{tempo};
        text.remove(0,2);
        ui->console->appendPlainText(text);

        //Vérifie si l'option "savelog" est coché et si oui, ecrit dans log.txt
        if(ui->saveLog->isChecked()){
            if(m_logFile.isOpen()){
                QTextStream stream(&m_logFile);
                stream << text << endl;
            }
        }
    }

    else if(data.front() == '1'){
        //Position
        //deplace l'image du robot sur la carte en fonction
        int x{stoi(data.substr(2, 4))};
        int y{stoi(data.substr(7, 4))};
        //TO DO.
        //ajustement a faire
        ui->Robot->move(x, y);
    }
    data.clear();
}


//Fonction permettant l'envoie de paquet au serveur.
void MainWindow::write(QByteArray msg){
    if(m_socket.write(msg) == -1)
        cout << "Error write" << endl;
}


//Fonction appelé en appuyant sur la bouton "Stop/Start" de l'interface.
void MainWindow::startStop(){
    QByteArray data;
    if(m_isRunning){
        ui->StopButton->setText("Start");
        data.push_front(CMD_STOP);
        write(data);
        m_isRunning = false;
    }
    else{
        ui->StopButton->setText("Stop");
        data.push_front(CMD_START);
        write(data);
        m_isRunning = true;
    }
}


//Fonction appelé à chaque appuie sur la bouton "Auto/Manual" de l'interface.
void MainWindow::switchMode(){
    QByteArray data;
    if(m_autoMode){
        ui->Mode->setText("Manual");
        data.push_front(MODE_AUTO);
        write(data);
        m_autoMode = false;
    }
    else{
        ui->Mode->setText("Auto");
        data.push_front(MODE_MANUAL);
        write(data);
        m_autoMode = true;
    }
}


//Fonction appelé à chaque appuie sur la bouton "Connect" de l'interface.
void MainWindow::reconnect(){
    m_socket.close();
    m_socket.connectToHost(QHostAddress(m_IPAddress), m_TCPPORT);
};


//Fonction en charge d'afficher le statue de la connection avec le serveur.
//Est appelé à chaque fois que m_socket change d'état (voir les "connect" dans le constructeur).
void MainWindow::switchState(){
    switch(m_socket.state()){
        case QAbstractSocket::UnconnectedState :
            ui->VariableState->setText("Unconnected");
            break;
        case QAbstractSocket::HostLookupState :
            ui->VariableState->setText("Host Lookup");
            break;
        case QAbstractSocket::ConnectingState :
            ui->VariableState->setText("Connecting");
            break;
        case QAbstractSocket::ConnectedState :
            ui->VariableState->setText("Connected");
            break;
        case QAbstractSocket::BoundState :
            ui->VariableState->setText("Bound");
            break;
        case QAbstractSocket::ListeningState :
            ui->VariableState->setText("Listening");
            break;
        case QAbstractSocket::ClosingState :
            ui->VariableState->setText("Closing");
            break;
        default:
            break;
    };
};


//Fonction appelé lorsque la saisie d'une nouvelle addresse IP est terminé (à l'appuie sur la touche "Enter").
void MainWindow::modifyIP(){
    m_IPAddress = ui->GetIP->text();
    ui->IPUsed->setText(m_IPAddress);
    ui->GetIP->clear();
    ui->GetIP->setEnabled(false); //désactive la zone de saisie jusqu'à ce que l'utilisateur clique dessus.
};


//Fonction appelé lorsque la checkbox "Save log" est coché ou décoché.
void MainWindow::saveLogFonction(){
    if(ui->saveLog->isChecked()){
        if(m_logFile.open((QIODevice::WriteOnly | QIODevice::Text))){
            QTextStream stream(&m_logFile);
            stream << ui->console->toPlainText() << endl;
        }
    }
    else{
        m_logFile.close();
    }
};


